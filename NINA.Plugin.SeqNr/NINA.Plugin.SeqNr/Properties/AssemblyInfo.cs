﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: Guid("7f14fd8b-384f-41f7-a1ad-c0f88698ff46")]
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.2")]

[assembly: AssemblyTitle("Starting Sequence Number")]
[assembly: AssemblyDescription("Plugin to enable starting a file index sequencing numbering at a specified value.")]

[assembly: AssemblyCompany("Simon Kapadia")]
[assembly: AssemblyProduct("NINA.Plugin.SeqNr")]
[assembly: AssemblyCopyright("Copyright ©2021")]

[assembly: AssemblyMetadata("MinimumApplicationVersion", "2.0.0.2001")]

[assembly: AssemblyMetadata("License", "MPL-2.0")]
[assembly: AssemblyMetadata("LicenseURL", "https://www.mozilla.org/en-US/MPL/2.0/")]
[assembly: AssemblyMetadata("Repository", "https://bitbucket.org/MrSzymon/nina.plugin.seqnr/")]
[assembly: AssemblyMetadata("Homepage", "http://astrophotos.uk")]
[assembly: AssemblyMetadata("Tags", "Template,Sequencer")]

[assembly: AssemblyMetadata("ChangelogURL", "https://bitbucket.org/MrSzymon/nina.plugin.seqnr/commits/branch/master")]
[assembly: AssemblyMetadata("FeaturedImageURL", "https://bitbucket.org/MrSzymon/nina.plugin.seqnr/downloads/SeqNrLogo.png")]
[assembly: AssemblyMetadata("ScreenshotURL", "https://bitbucket.org/MrSzymon/nina.plugin.seqnr/downloads/SeqNr.jpg")]
[assembly: AssemblyMetadata("LongDescription", @"This plugin allows you to continue a previous night's imaging by starting your file sequence numbering from a specified number.
For example, if your previous nights last good image was *\*_0071.xisf*, you can specify *72* here and the next image will be saved
with index number *\*_0072.xisf*.  Note that this value is actualy a simple offset from the default index, 0.

The plugin lives under the 'Utility' category and should be dragged to the start of a container where there are TakeExposure, 
TakeManyExposures or SmartExposure instructions.  Any files saved by these instructions will then use the new begining index
for sequence numbering.
")]